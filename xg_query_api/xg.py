import requests
import os
import time
import json
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

from .token import Token

class API:

    def auth_request(self, url):

        access_token = self.token.get_token()['accessToken']
        headers = {
            'Authorization': 'Bearer ' + access_token
        }

        r = requests.get(
            self.base + url, 
            headers = headers, 
            verify = False,
            timeout = self.timeout
        )

        if r.status_code == 200:
            return r.json()
        else:
            raise Exception(r.text)


    def auth_put(self, url, body):

        access_token = self.token.get_token()['accessToken']
        headers = {
            'Authorization': 'Bearer ' + access_token
        }

        r = requests.put(
            self.base + url, 
            headers = headers, 
            verify = False,
            json = body,
            timeout = self.timeout
        )

        if r.status_code == 200:
            return r.json()
        else:
            raise Exception(r.text)


    def auth_post(self, url, body):

        access_token = self.token.get_token()['accessToken']
        headers = {
            'Authorization': 'Bearer ' + access_token
        }

        r = requests.post(
            self.base + url, 
            headers = headers, 
            verify = False,
            json = body,
            timeout = self.timeout
        )

        if r.status_code == 200:
            return r.json()
        else:
            raise Exception(r.text)


    def request(self, url):
        r = requests.get(
            self.base + url,
            timeout = self.timeout
        )
        return r.json()


    def __init__(self, email=None, pwd=None, timeout=20):

        if os.environ.get('XG_LOCATION'):
            self.base = os.environ.get('XG_LOCATION')
        else:
            self.base = "https://api.xg.football"

        self.timeout = timeout
        self.token = Token(email, pwd)
        return
 
