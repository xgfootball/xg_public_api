import time
import json
import pandas as pd

from xg_query_api.core.getter import Getter

class Getters:

    def get_current_active_seasons(self):
        url = '/tournaments/current'
        return self.getter.fetch(url)

    def get_match_by_match(self, match_id):
        url = '/match/' + str(match_id)
        return self.getter.fetch(url)

    def get_xgmatchstats_by_match(self, match_id):
        url = '/match/playerstats/' + str(match_id)
        return self.getter.fetch(url)

    def get_liveodds_by_match(self, match_id):
        url = '/match/liveodds/' + str(match_id)
        return self.getter.fetch(url)

    def get_shots_by_match(self, match_id):
        url = '/match/shots/' + str(match_id)
        return self.getter.fetch(url)

    def recent_matches(self):
        url = '/matches/recent'
        return self.getter.fetch(url)

    def upcoming_matches(self):
        url = '/matches/upcoming'
        return self.getter.fetch(url)

    def get_xgmatchstats_by_season(self, season_id):
        url = '/matchstats/get?seasonid=' + str(season_id)
        return self.getter.fetch(url)

    def get_xgteamstats_by_match(self, match_id):
        url = '/match/teamstats/' + str(match_id)
        return self.getter.fetch(url)

    def get_xgteamstats_by_season(self, season_id):
        url = '/season/teamstats/' + str(season_id)
        return self.getter.fetch(url)
     
    def get_xgteamstats_by_tournament(self, tournament_id):
        url = '/tournament/teamstats/' + str(tournament_id)
        return self.getter.fetch(url)

    def get_player_averages(self, player_id):
        url = '/player/averages/' + str(player_id)
        return self.getter.fetch(url)

    def get_player_percentile(self, player_id):
        url = '/player/percentile/' + str(player_id)
        return self.getter.fetch(url)

    def get_player_upcoming_matches(self, player_id):
        url = '/player/upcomingmatches/' + str(player_id)
        return self.getter.fetch(url)
    
    def get_player_teamaverages(self, player_id):
        ##For the player's club team in league tournaments
        url = '/player/teamaveragesmain/' + str(player_id)
        return self.getter.fetch(url)

    def get_player_latest_season(self, player_id):
        url = '/player/latestseasonstats/' + str(player_id)
        return self.getter.fetch(url)

    def get_player_team_history(self, player_id):
        url = '/player/history/' + str(player_id)
        return self.getter.fetch(url)

    def get_player_shots(self, player_id):
        url = '/player/shots/' + str(player_id)
        return self.getter.fetch(url)

    def get_season_averages(self, season_id):
        url = '/season/averages/' + str(season_id)
        return self.getter.fetch(url)

    def get_season_top_players_by_season(self, season_id):
        #This only works across a limited set of categories
        url = '/season/topplayers/' + str(season_id)
        return self.getter.fetch(url)
 
    def get_season_league_table(self, season_id):
        url = '/season/leaguetable/' + str(season_id)
        return self.getter.fetch(url)
 
    def get_season_rolling_averages(self, season_id):
        url = '/season/averagesrolling/' + str(season_id)
        return self.getter.fetch(url)
 
    def get_matches_by_season(self, season_id):
        url = '/season/matches/' + str(season_id)
        return self.getter.fetch(url)
 
    def get_team_averages(self, team_id):
        url = '/team/averages/' + str(team_id)
        return self.getter.fetch(url)
 
    def get_team_rolling_averages(self, team_id):
        url = '/team/averagesolling/' + str(team_id)
        return self.getter.fetch(url)
 
    def get_player_averages_by_team(self, team_id):
        url = '/team/playeraverages/' + str(team_id)
        return self.getter.fetch(url) 
 
    def get_team_league_rank(self, team_id):
        url = '/team/leaguerank/' + str(team_id)
        return self.getter.fetch(url)
 
    def get_team_league_percentile(self, team_id):
        url = '/team/leaguepercentile/' + str(team_id)
        return self.getter.fetch(url)
 
    def get_team_current_league_rank(self, team_id):
        url = '/team/currentleaguerank/' + str(team_id)
        return self.getter.fetch(url)
 
    def get_team_upcoming_matches(self, team_id):
        url = '/team/upcomingmatches/' + str(team_id)
        return self.getter.fetch(url)
 
    def get_team_latest_stats(self, team_id):
        url = '/team/latestseasonstats/' + str(team_id)
        return self.getter.fetch(url)
 
    def get_team_shots(self, team_id):
        url = '/team/shots/' + str(team_id)
        return self.getter.fetch(url)
 
    def get_tournament_current_matches(self, tourn_id):
        url = '/tournament/currentmatches/' + str(tourn_id)
        return self.getter.fetch(url)
 
    def get_tournament_current_league_table(self, tourn_id):
        url = '/tournament/currentleaguetable/' + str(tourn_id)
        return self.getter.fetch(url)
 
    def get_tournament_current_top_players(self, tourn_id):
        url = '/tournament/currenttopplayers/' + str(tourn_id)
        return self.getter.fetch(url)
 
    def get_tournament_seasons(self, tourn_id):
        url = '/tournament/seasons/' + str(tourn_id)
        return self.getter.fetch(url)
 
    def get_tournament_league_table(self, tourn_id):
        url = '/tournament/leaguetable/' + str(tourn_id)
        return self.getter.fetch(url)
 
    def get_tournaments_all(self):
        url = '/tournaments/all'
        return self.getter.fetch(url)
 
    def get_tournaments_current(self):
        url = '/tournaments/current'
        return self.getter.fetch(url)
 
    def get_tournaments_live(self):
        url = '/tournaments/live'
        return self.getter.fetch(url)

    @staticmethod
    def to_json(data, location):
        with open(location, 'w') as f:
             json.dump(data, f)
        return
 
    @staticmethod
    def to_csv(data, location):
        df = pd.DataFrame(data)
        df.to_csv(location)
        return

    def __init__(self, email=None, pwd=None):
        self.getter = Getter(email, pwd)
        return    
