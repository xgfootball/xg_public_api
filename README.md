

## How to install this package

When you are inside the package run:
```
python setup.py install
pip install .
```

You can then import from xg\_query\_api in any of your code on the same system.

## How to use this package

This package provides a simple interface around our api, and will serve as documentation for any changes that we make to the api.
```
from xg_query_api.getters.getters import Getters
```

The main interface to the api is the Getters class. All the routes that are supported by the api are methods on this class.
```
Getters().get_tournaments_live()
```

To see what methods are supported, you can run:
```
dir(Getters())
```
or check the getters folder in this repo.

If you are a subscriber, you can authenticate by either providing an XG\_EMAIL and XG\_PWD environment variable or including your email and password to the Getters constructor.
```
email = ''
pwd = ''
g = Getters(email,pwd)
g.get_tournaments_live()
```

To output the results to json or csv you can use static methods on Getters:
```
tourns = Getters().get_tournaments_live()
Getters.to_json(tourns, 'tourns.json')
Getters.to_csv(tourns, 'tourns.csv')
```
