from setuptools import setup, find_packages

setup(
    name="xg_query_api",
    version="0.1",
    packages=find_packages(),
    setup_requires=[
        'urllib3==1.23'
    ],
    install_requires=[
        'requests',
        'pandas',
        'urllib3==1.23'
    ]
)

