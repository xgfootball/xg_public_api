
---
Paper Title
Author
Journal - Issue - Date
DOI
Abstract

---

Regression models for forecasting and matches results in association football\
John Goddard\
International Journal of Forecasting - 21 - 2005\
10.1016/j.ijforecast.2004.08.002\
In the previous literature, two approaches have been used to model match outcomes in association football (soccer): first, modelling the goals scored and conceded by each team; and second, modelling win–draw–lose match results directly. There have been no previous attempts to compare the forecasting performance of these two types of model. This paper aims to fill this gap. Bivariate Poisson regression is used to estimate forecasting models for goals scored and conceded. Ordered probit regression is used to estimate forecasting models for match results. Both types of models are estimated using the same 25-year data set on English league football match outcomes. The best forecasting performance is achieved using a dhybridT specification, in which goals-based team performance covariates are used to forecast win–draw–lose match results. However, the differences between the forecasting performance of models based on goals data and models based on results data appear to be relatively small.

---

On the dependency of soccer scores – a sparse bivariate Poisson model for the UEFA European football championship 2016\
Andreas Groll, Thomas Kneib, Andreas Mayr and Gunther Schauberger\
Journal of Quanatiative Analysis in Sports - - 2018\
10.1515/jqas-2017-0067\
When analyzing and modeling the results of soccer matches, one important aspect is to account for the correct dependence of the scores of two competing teams. Several studies have found that, marginally, these scores are moderately negatively correlated. Even though many approaches that analyze the results of soccer matches are based on two (conditionally) independent pairwise Poisson distributions, a certain amount of (mostly neg- ative) dependence between the scores of the competing teams can simply be induced by the inclusion of covari- ate information of both teams in a suitably structured lin- ear predictor. One objective of this article is to analyze if this type of modeling is appropriate or if additional explicit modeling of the dependence structure for the joint score of a soccer match needs to be taken into account. Therefore, a specific bivariate Poisson model for the two numbers of goals scored by national teams competing in UEFA European football championship matches is fitted to all matches from the three previous European championships, including covariate information of both competing teams. A boosting approach is then used to select the relevant covariates. Based on the estimates, the tournament is simulated 1,000,000 times to obtain winning probabilities for all participating national teams. 

---

Prediction of major international soccer tournaments based on team-specific regularized Poisson regression: An application to the FIFA World Cup 2014\
Andreas Groll, Gunther Schauberger and Gerhard Tutz\
Journal of Quanatiative Analysis in Sports - - 2017\
In this article an approach for the analysis and prediction of international soccer match results is proposed. It is based on a regularized Poisson regres- sion model that includes various potentially influential covariates describing the national teams’ success in pre- vious FIFA World Cups. Additionally, within the general- ized linear model (GLM) framework, also differences of team-specific effects are incorporated. In order to achieve variable selection and shrinkage, we use tailored Lasso approaches. Based on preceding FIFA World Cups, two models for the prediction of the FIFA World Cup 2014 are fitted and investigated. Based on the model estimates, the FIFA World Cup 2014 is simulated repeatedly and winning probabilities are obtained for all teams. Both models favor the actual FIFA World Champion Germany. 

---

Ajay Andrew Gupta\
A new approach to bracket prediction in the NCAA Men’s Basketball Tournament based on a dual- proportion likelihood \
Journal of Quantative Analysis in Sports - 11 - 2015\
10.1515/jqas-2014-0047\
The widespread proliferation of and interest in bracket pools that accompany the National Collegiate Ath- letic Association Division I Men’s Basketball Tournament have created a need to produce a set of predicted winners for each tournament game by people without expert knowl- edge of college basketball. Previous research has addressed bracket prediction to some degree, but not nearly on the level of the popular interest in the topic. This paper reviews relevant previous research, and then introduces a rating sys- tem for teams using game data from that season prior to the tournament. The ratings from this system are used within a novel, four-predictor probability model to produce sets of bracket predictions for each tournament from 2009 to 2014. This dual-proportion probability model is built around the constraint of two teams with a combined 100% probability of winning a given game. This paper also performs Monte Carlo simulation to investigate whether modifications are necessary from an expected value-based prediction system such as the one introduced in the paper, in order to have the maximum bracket score within a defined group. The find- ings are that selecting one high-probability “upset” team for one to three late rounds games is likely to outperform other strategies, including one with no modifications to the expected value, as long as the upset choice overlaps a large minority of competing brackets while leaving the bracket some distinguishing characteristics in late rounds. 

---

Sameer K. Deshpande and Shane T. Jensen\
Estimating an NBA player’s impact on his team’s chances of winning\
Journal of Quantitative Analysis in Sports - - 2016\
10.1515/jqas-2015-0027\
Traditional NBA player evaluation metrics are based on scoring differential or some pace-adjusted linear combination of box score statistics like points, rebounds, assists, etc. These measures treat performances with the outcome of the game still in question (e.g. tie score with five minutes left) in exactly the same way as they treat per- formances with the outcome virtually decided (e.g. when one team leads by 30 points with one minute left). Because they ignore the context in which players perform, these measures can result in misleading estimates of how play- ers help their teams win. We instead use a win probability framework for evaluating the impact NBA players have on their teams’ chances of winning. We propose a Bayesian linear regression model to estimate an individual play- er’s impact, after controlling for the other players on the court. We introduce several posterior summaries to derive rank-orderings of players within their team and across the league. This allows us to identify highly paid players with low impact relative to their teammates, as well as players whose high impact is not captured by existing metrics. 

---

Guanhao Feng, Nicholas Polson and Jianeng Xu\
The market for English Premier League (EPL) odds\
Journal of Quantiative Analysis in Sports - - 2017\
10.1515/jqas-2016-0039\
This paper employs a Skellam process to rep- resent real-time betting odds for English Premier League (EPL) soccer games. Given a matrix of market odds on all possible score outcomes, we estimate the expected scoring rates for each team. The expected scoring rates then define the implied volatility of an EPL game. As events in the game evolve, we re-estimate the expected scoring rates and our implied volatility measure to provide a dynamic representation of the market’s expectation of the game outcome. Using a dataset of 1520 EPL games from 2012– 2016, we show how our model calibrates well to the game outcome. We illustrate our methodology on real-time mar- ket odds data for a game between Everton and West Ham in the 2015–2016 season. We show how the implied volatility for the outcome evolves as goals, red cards, and corner kicks occur. Finally, we conclude with directions for future research. 

---

Josip Hucaljuk, Alen Rakipović\
Predicting football scores using machine learning techniques\
MIPRO 2011\
None\
Predicting the results of football matches poses an interesting challenge due to the fact that the sport is so popular and widespread. However, predicting the outcomes is also a difficult problem because of the number of factors which must be taken into account that cannot be quantitatively valued or modeled. As part of this work, a software solution has been developed in order to try and solve this problem. During the development of the system, a number of tests have been carried out in order to determine the optimal combination of features and classifiers. The results of the presented system show a satisfactory capability of prediction which is superior to the one of the reference method (most likely a priori outcome). 

---

Samuel E. Buttrey\
Beating the market betting on NHL hockey games\
Journal of Quantitative Analysis in Sports - - 2016\
10.1515/jqas-2015-0003\
This article describes a method for predicting the outcome of National Hockey League (NHL) games. We combine a model for goal scoring and yielding, and one for penalty commission, in a Markov-type computation and a simulation model that produce predicted probabilities of victory for each team. Where these differ substantially from the market probabilities, we make “bets” according to a simple strategy. Our return on investment is both posi- tive and statistically significant. 

---

Anthony Costa Constantinou and Norman Elliott Fenton\
Solving the Problem of Inadequate Scoring Rules for Assessing Probabilistic Football Forecast Models\
Journal of Quantitative Analysis in Sports\
Manuscript 1418\
Despite the massive popularity of probabilistic (association) football forecasting models, and the relative simplicity of the outcome of such forecasts (they require only three probability values corresponding to home win, draw, and away win) there is no agreed scoring rule to determine their forecast accuracy. Moreover, the various scoring rules used for validation in previous studies are inadequate since they fail to recognise that football outcomes represent a ranked (ordinal) scale. This raises severe concerns about the validity of conclusions from previous studies. There is a well-established generic scoring rule, the Rank Probability Score (RPS), which has been missed by previous researchers, but which properly assesses football forecasting models. 

---

Timothy C. Y. Chan and Raghav Singal\
A Markov Decision Process-based handicap system for tennis\
Journal of Quantitative Analysis in Sports - - 2017\
10.1515/jqas-2016-0057\
Handicap systems are used in many sports to improve competitive balance and equalize the match-win probability between opponents of differing ability. Recog- nizing the absence of such a system in tennis, we develop a novel optimization-based handicap system for tennis using a Markov Decision Process (MDP) model. In our handicap system, the weaker player is given β “free points” or “credits” at the start of the match, which he can use be- fore the start of any point during the match to win the point outright. The MDP model determines two key features of the handicap system: (1) Fairness: the minimum value of β required to equalize the match-win probability, and (2) Achievability: the optimal policy governing usage of the β credits to achieve the desired match-win probability. We test the sensitivity of the handicap values to the model’s in- put parameters. Finally, we apply the model to real match data to estimate professional handicaps. 

---

Iavor Bojinov and Luke Bornn\
The Pressing Game: Optimal Defensive Disruption in Soccer\
MIT Sloan Sports Analytics Conference 2016\
None\
Soccer, the most watched sport in the world, is a dynamic game where a team’s success relies on both team strategy and individual player contributions. Passing is a cardinal soccer skill and a key factor in strategy de- velopment; it helps the team to keep the ball in its possession, move it across the field, and outmaneuver the opposing team in order to score a goal. From a defensive perspective, however, it is just as important to stop passes from happening, thereby disrupting the opposing team’s flow of play. Our main contribution utilizes this fundamental observation to define and learn a spatial map of each team’s defensive weaknesses and strengths. Moreover, as a byproduct of this approach we also obtain a team specific offensive control surface, which de- scribes a team’s ability to retain possession in different regions of the field. Our results can be used to distin- guish between different defensive strategies, such as pressing high up the field or sitting back, as well as specifc player contributions and the impact of a manager. 

---

Lotte Bransen and Jan Van Haaren\
Measuring football players’ on-the-ball contributions from passes during games\
None\
None\
Several performance metrics for quantifying the in-game per- formances of individual football players have been proposed in recent years. Although the majority of the on-the-ball actions during games constitutes of passes, many of the currently available metrics focus on measuring the quality of shots only. To help bridge this gap, we propose a novel approach to measure players’ on-the-ball contributions from passes during games. Our proposed approach measures the expected impact of each pass on the scoreline. 

---

Rose D. Baker, Ian G. McHale\
Time-varying ratings for international football teams\
European Journal of Operational Research - - 2017\
10.1016/j.ejor.2017.11.042\
We present a model for rating international football teams. Using data from 1944 to 2016, we ask ‘which was the greatest team?’. To answer the question requires some sophisticated methodology. Specifically, we have used k-fold cross-validation, which allows us to optimally down-weight the results of friendly matches in explaining World Cup results. In addition to the central aim of the paper, we also discuss, from a philosophical perspective, situations in which model over-fitting is perhaps desirable. Results suggest that Hungary in 1952, is a strong candidate for the all-time greatest international football team. 

---

Rose D. Baker, Ian G. McHale\
Optimal Betting Under Parameter Uncertainty: Improving the Kelly Criterion\
Decision Analysis - 10 - 2013\
10.1287/deca.2013.0271\
The Kelly betting criterion ignores uncertainty in the probability of winning the bet and uses an estimated probability. In general, such replacement of population parameters by sample estimates gives poorer out- of-sample than in-sample performance. We show that to improve out-of-sample performance the size of the bet should be shrunk in the presence of this parameter uncertainty, and compare some estimates of the shrinkage factor. From a simulation study and from an analysis of some tennis betting data we show that the shrunken Kelly approaches developed here offer an improvement over the “raw” Kelly criterion. One approximate esti- mate of the shrinkage factor gives a “back of envelope” correction to the Kelly criterion that could easily be used by bettors. We also study bet shrinkage and swelling for general risk-averse utility functions and discuss the general implications of such results for decision theory 

---

Muhammad Asif, Ian G. McHale\
In-play forecasting of win probability in One-Day International cricket: A dynamic logistic regression model\
International Journal of Forecasting - 32 - 2016\
10.1016/j.ijforecast.2015.02.005\
The paper presents a model for forecasting the outcomes of One-Day International cricket matches whilst the game is in progress. Our ‘in-play’ model is dynamic, in the sense that the parameters of the underlying logistic regression model are allowed to evolve smoothly as the match progresses. The use of this dynamic logistic regression approach reduces the number of parameters required dramatically, produces stable and intuitive forecast probabilities, and has a minimal effect on the explanatory power. Cross-validation techniques are used to identify the variables to be included in the model. We demonstrate the use of our model using two matches as examples, and compare the match result probabilities generated using our model with those from the betting market. The forecasts are similar quantitatively, a result that we take to be evidence that our modelling approach is appropriate. 

---

Paul Power, Xinyu Wei, Patrick Lucey, Hector Ruiz\
“Not All Passes Are Created Equal:” Objectively Measuring the Risk and Reward of Passes in Soccer from Tracking Data \
Proceedings of the 23rd ACM SIGKDD International Conference on Knowledge Discovery and Data Mining\
10.475/123 4\
In soccer, the most frequent event that occurs is a pass. For a trained eye, there are a myriad of adjectives which could describe this event (e.g., “majestic pass”, “conservative” to “poor-ball”). However, as these events are needed to be coded live and in real-time (most often by human annotators), the current method of grading passes is restricted to the binary labels 0 (unsuccessful) or 1 (successful). Obviously, this is sub-optimal because the quality of a pass needs to be measured on a continuous spectrum (i.e., 0 ! 100%) and not a binary value. Additionally, a pass can be measured across multiple dimensions, namely: i) risk – the likelihood of executing a pass in a given situation, and ii) reward – the likelihood of a pass creating a chance. In this paper, we show how we estimate both the risk and reward of a pass across two seasons of tracking data captured from a recent professional soccer league with state-of-the- art performance, then showcase various use cases of our deployed passing system. 
